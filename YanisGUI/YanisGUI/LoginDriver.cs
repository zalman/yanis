﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace YanisGUI
{
    class LoginDriver
    {
        private string chromeDriverLocation = @"C:\Users\Manu\Downloads\chromedriver_win32\";
        private string firefoxPath = (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe", "", null)).ToString();

        public void login(string baseUrl, string username, string password,
            Dictionary<string, string> xpaths)
        {

            FirefoxProfile profile = new FirefoxProfile();
            profile.SetPreference("webdriver.firefox.bin", this.firefoxPath);
            IWebDriver driver = new FirefoxDriver(profile);
            driver.Manage().Window.Maximize();

            driver.Navigate().GoToUrl(baseUrl);

            driver.FindElement(By.XPath(xpaths["username_input"])).Clear();
            driver.FindElement(By.XPath(xpaths["username_input"])).SendKeys(username);

            driver.FindElement(By.XPath(xpaths["password_input"])).Clear();
            driver.FindElement(By.XPath(xpaths["password_input"])).SendKeys(password);

            driver.FindElement(By.XPath(xpaths["submit_btn_input"])).Click();
        }
    }
}
