﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace YanisGUI
{
    public static class PasswordManager
    {
        private static Encoder enc;
        private static byte[] unicodeText;
        private static MD5 md5;
        private static byte[] result;
        private static StringBuilder sb;

        public static string EncryptPassword(string str)
        {
            //getMd5Sum;
            enc = System.Text.Encoding.Unicode.GetEncoder();
            unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            md5 = new MD5CryptoServiceProvider();
            result = md5.ComputeHash(unicodeText);

            sb = new StringBuilder();
            for (int i = 0; i < result.Length; ++i)
                sb.Append(result[i].ToString("X2"));

            return sb.ToString();
        }
    }
}
