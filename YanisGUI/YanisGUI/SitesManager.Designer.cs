﻿namespace YanisGUI
{
    partial class frmSitesManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSitesManager));
            this.tab_SitesManager = new System.Windows.Forms.TabControl();
            this.tabFacebook = new System.Windows.Forms.TabPage();
            this.btn_LMKeyFB = new System.Windows.Forms.Button();
            this.btn_CheckCredentialsFB = new System.Windows.Forms.Button();
            this.gb_FB = new System.Windows.Forms.GroupBox();
            this.tb_PasswdFB = new System.Windows.Forms.TextBox();
            this.tb_EmailFB = new System.Windows.Forms.TextBox();
            this.lbl_PasswdFB = new System.Windows.Forms.Label();
            this.lbl_EmailFB = new System.Windows.Forms.Label();
            this.tabGmail = new System.Windows.Forms.TabPage();
            this.tb_LMKeyGmail = new System.Windows.Forms.Button();
            this.tb_CheckCredentialsGmail = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_PasswdGmail = new System.Windows.Forms.TextBox();
            this.tb_UsernameGmail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabYahooMail = new System.Windows.Forms.TabPage();
            this.btn_LMKeyY = new System.Windows.Forms.Button();
            this.btn_CheckCredentialsY = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_PasswdY = new System.Windows.Forms.TextBox();
            this.tb_UsernameY = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabLinkedIn = new System.Windows.Forms.TabPage();
            this.btn_LMKeyLinkedin = new System.Windows.Forms.Button();
            this.btn_CheckCredentialsLinkedin = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tb_PasswdLinkedin = new System.Windows.Forms.TextBox();
            this.tb_EmailLinkedin = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabDropbox = new System.Windows.Forms.TabPage();
            this.btn_LMKeyDropbox = new System.Windows.Forms.Button();
            this.btn_CheckCredentialsDropbox = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tb_PasswdDropbox = new System.Windows.Forms.TextBox();
            this.tb_EmailDropbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ilst_Icons = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageSitesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tab_SitesManager.SuspendLayout();
            this.tabFacebook.SuspendLayout();
            this.gb_FB.SuspendLayout();
            this.tabGmail.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabYahooMail.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabLinkedIn.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabDropbox.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab_SitesManager
            // 
            this.tab_SitesManager.Controls.Add(this.tabFacebook);
            this.tab_SitesManager.Controls.Add(this.tabGmail);
            this.tab_SitesManager.Controls.Add(this.tabYahooMail);
            this.tab_SitesManager.Controls.Add(this.tabLinkedIn);
            this.tab_SitesManager.Controls.Add(this.tabDropbox);
            this.tab_SitesManager.ImageList = this.ilst_Icons;
            this.tab_SitesManager.Location = new System.Drawing.Point(12, 38);
            this.tab_SitesManager.Name = "tab_SitesManager";
            this.tab_SitesManager.SelectedIndex = 0;
            this.tab_SitesManager.Size = new System.Drawing.Size(355, 191);
            this.tab_SitesManager.TabIndex = 0;
            // 
            // tabFacebook
            // 
            this.tabFacebook.BackColor = System.Drawing.SystemColors.Control;
            this.tabFacebook.Controls.Add(this.btn_LMKeyFB);
            this.tabFacebook.Controls.Add(this.btn_CheckCredentialsFB);
            this.tabFacebook.Controls.Add(this.gb_FB);
            this.tabFacebook.ImageIndex = 0;
            this.tabFacebook.Location = new System.Drawing.Point(4, 23);
            this.tabFacebook.Name = "tabFacebook";
            this.tabFacebook.Padding = new System.Windows.Forms.Padding(3);
            this.tabFacebook.Size = new System.Drawing.Size(347, 164);
            this.tabFacebook.TabIndex = 0;
            this.tabFacebook.Text = "Facebook";
            // 
            // btn_LMKeyFB
            // 
            this.btn_LMKeyFB.Location = new System.Drawing.Point(179, 123);
            this.btn_LMKeyFB.Name = "btn_LMKeyFB";
            this.btn_LMKeyFB.Size = new System.Drawing.Size(129, 23);
            this.btn_LMKeyFB.TabIndex = 2;
            this.btn_LMKeyFB.Text = "Save";
            this.btn_LMKeyFB.UseVisualStyleBackColor = true;
            this.btn_LMKeyFB.Click += new System.EventHandler(this.btn_LMKeyFB_Click);
            // 
            // btn_CheckCredentialsFB
            // 
            this.btn_CheckCredentialsFB.Location = new System.Drawing.Point(36, 123);
            this.btn_CheckCredentialsFB.Name = "btn_CheckCredentialsFB";
            this.btn_CheckCredentialsFB.Size = new System.Drawing.Size(129, 23);
            this.btn_CheckCredentialsFB.TabIndex = 1;
            this.btn_CheckCredentialsFB.Text = "Check Credentials";
            this.btn_CheckCredentialsFB.UseVisualStyleBackColor = true;
            // 
            // gb_FB
            // 
            this.gb_FB.Controls.Add(this.tb_PasswdFB);
            this.gb_FB.Controls.Add(this.tb_EmailFB);
            this.gb_FB.Controls.Add(this.lbl_PasswdFB);
            this.gb_FB.Controls.Add(this.lbl_EmailFB);
            this.gb_FB.Location = new System.Drawing.Point(36, 6);
            this.gb_FB.Name = "gb_FB";
            this.gb_FB.Size = new System.Drawing.Size(272, 111);
            this.gb_FB.TabIndex = 0;
            this.gb_FB.TabStop = false;
            // 
            // tb_PasswdFB
            // 
            this.tb_PasswdFB.Location = new System.Drawing.Point(65, 61);
            this.tb_PasswdFB.Name = "tb_PasswdFB";
            this.tb_PasswdFB.Size = new System.Drawing.Size(184, 20);
            this.tb_PasswdFB.TabIndex = 3;
            // 
            // tb_EmailFB
            // 
            this.tb_EmailFB.Location = new System.Drawing.Point(65, 23);
            this.tb_EmailFB.Name = "tb_EmailFB";
            this.tb_EmailFB.Size = new System.Drawing.Size(184, 20);
            this.tb_EmailFB.TabIndex = 2;
            // 
            // lbl_PasswdFB
            // 
            this.lbl_PasswdFB.AutoSize = true;
            this.lbl_PasswdFB.Location = new System.Drawing.Point(6, 68);
            this.lbl_PasswdFB.Name = "lbl_PasswdFB";
            this.lbl_PasswdFB.Size = new System.Drawing.Size(53, 13);
            this.lbl_PasswdFB.TabIndex = 1;
            this.lbl_PasswdFB.Text = "Password";
            // 
            // lbl_EmailFB
            // 
            this.lbl_EmailFB.AutoSize = true;
            this.lbl_EmailFB.Location = new System.Drawing.Point(6, 30);
            this.lbl_EmailFB.Name = "lbl_EmailFB";
            this.lbl_EmailFB.Size = new System.Drawing.Size(32, 13);
            this.lbl_EmailFB.TabIndex = 0;
            this.lbl_EmailFB.Text = "Email";
            this.lbl_EmailFB.Click += new System.EventHandler(this.label1_Click);
            // 
            // tabGmail
            // 
            this.tabGmail.BackColor = System.Drawing.SystemColors.Control;
            this.tabGmail.Controls.Add(this.tb_LMKeyGmail);
            this.tabGmail.Controls.Add(this.tb_CheckCredentialsGmail);
            this.tabGmail.Controls.Add(this.groupBox2);
            this.tabGmail.ImageIndex = 1;
            this.tabGmail.Location = new System.Drawing.Point(4, 23);
            this.tabGmail.Name = "tabGmail";
            this.tabGmail.Padding = new System.Windows.Forms.Padding(3);
            this.tabGmail.Size = new System.Drawing.Size(347, 164);
            this.tabGmail.TabIndex = 1;
            this.tabGmail.Text = "Gmail";
            // 
            // tb_LMKeyGmail
            // 
            this.tb_LMKeyGmail.Location = new System.Drawing.Point(179, 123);
            this.tb_LMKeyGmail.Name = "tb_LMKeyGmail";
            this.tb_LMKeyGmail.Size = new System.Drawing.Size(129, 23);
            this.tb_LMKeyGmail.TabIndex = 4;
            this.tb_LMKeyGmail.Text = "Save";
            this.tb_LMKeyGmail.UseVisualStyleBackColor = true;
            this.tb_LMKeyGmail.Click += new System.EventHandler(this.tb_LMKeyGmail_Click);
            // 
            // tb_CheckCredentialsGmail
            // 
            this.tb_CheckCredentialsGmail.Location = new System.Drawing.Point(36, 123);
            this.tb_CheckCredentialsGmail.Name = "tb_CheckCredentialsGmail";
            this.tb_CheckCredentialsGmail.Size = new System.Drawing.Size(129, 23);
            this.tb_CheckCredentialsGmail.TabIndex = 3;
            this.tb_CheckCredentialsGmail.Text = "Check Credentials";
            this.tb_CheckCredentialsGmail.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_PasswdGmail);
            this.groupBox2.Controls.Add(this.tb_UsernameGmail);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(36, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(272, 111);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // tb_PasswdGmail
            // 
            this.tb_PasswdGmail.Location = new System.Drawing.Point(65, 61);
            this.tb_PasswdGmail.Name = "tb_PasswdGmail";
            this.tb_PasswdGmail.Size = new System.Drawing.Size(184, 20);
            this.tb_PasswdGmail.TabIndex = 3;
            // 
            // tb_UsernameGmail
            // 
            this.tb_UsernameGmail.Location = new System.Drawing.Point(65, 23);
            this.tb_UsernameGmail.Name = "tb_UsernameGmail";
            this.tb_UsernameGmail.Size = new System.Drawing.Size(184, 20);
            this.tb_UsernameGmail.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Username";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tabYahooMail
            // 
            this.tabYahooMail.BackColor = System.Drawing.SystemColors.Control;
            this.tabYahooMail.Controls.Add(this.btn_LMKeyY);
            this.tabYahooMail.Controls.Add(this.btn_CheckCredentialsY);
            this.tabYahooMail.Controls.Add(this.groupBox3);
            this.tabYahooMail.ImageIndex = 2;
            this.tabYahooMail.Location = new System.Drawing.Point(4, 23);
            this.tabYahooMail.Name = "tabYahooMail";
            this.tabYahooMail.Size = new System.Drawing.Size(347, 164);
            this.tabYahooMail.TabIndex = 2;
            this.tabYahooMail.Text = "YMail";
            this.tabYahooMail.Click += new System.EventHandler(this.tabYahooMail_Click);
            // 
            // btn_LMKeyY
            // 
            this.btn_LMKeyY.Location = new System.Drawing.Point(179, 123);
            this.btn_LMKeyY.Name = "btn_LMKeyY";
            this.btn_LMKeyY.Size = new System.Drawing.Size(129, 23);
            this.btn_LMKeyY.TabIndex = 7;
            this.btn_LMKeyY.Text = "Save";
            this.btn_LMKeyY.UseVisualStyleBackColor = true;
            this.btn_LMKeyY.Click += new System.EventHandler(this.btn_LMKeyY_Click);
            // 
            // btn_CheckCredentialsY
            // 
            this.btn_CheckCredentialsY.Location = new System.Drawing.Point(36, 123);
            this.btn_CheckCredentialsY.Name = "btn_CheckCredentialsY";
            this.btn_CheckCredentialsY.Size = new System.Drawing.Size(129, 23);
            this.btn_CheckCredentialsY.TabIndex = 6;
            this.btn_CheckCredentialsY.Text = "Check Credentials";
            this.btn_CheckCredentialsY.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_PasswdY);
            this.groupBox3.Controls.Add(this.tb_UsernameY);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(36, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 111);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            // 
            // tb_PasswdY
            // 
            this.tb_PasswdY.Location = new System.Drawing.Point(65, 61);
            this.tb_PasswdY.Name = "tb_PasswdY";
            this.tb_PasswdY.Size = new System.Drawing.Size(184, 20);
            this.tb_PasswdY.TabIndex = 3;
            // 
            // tb_UsernameY
            // 
            this.tb_UsernameY.Location = new System.Drawing.Point(65, 23);
            this.tb_UsernameY.Name = "tb_UsernameY";
            this.tb_UsernameY.Size = new System.Drawing.Size(184, 20);
            this.tb_UsernameY.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Password";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Username";
            // 
            // tabLinkedIn
            // 
            this.tabLinkedIn.BackColor = System.Drawing.SystemColors.Control;
            this.tabLinkedIn.Controls.Add(this.btn_LMKeyLinkedin);
            this.tabLinkedIn.Controls.Add(this.btn_CheckCredentialsLinkedin);
            this.tabLinkedIn.Controls.Add(this.groupBox4);
            this.tabLinkedIn.ImageIndex = 3;
            this.tabLinkedIn.Location = new System.Drawing.Point(4, 23);
            this.tabLinkedIn.Name = "tabLinkedIn";
            this.tabLinkedIn.Size = new System.Drawing.Size(347, 164);
            this.tabLinkedIn.TabIndex = 3;
            this.tabLinkedIn.Text = "Linkedin";
            // 
            // btn_LMKeyLinkedin
            // 
            this.btn_LMKeyLinkedin.Location = new System.Drawing.Point(179, 123);
            this.btn_LMKeyLinkedin.Name = "btn_LMKeyLinkedin";
            this.btn_LMKeyLinkedin.Size = new System.Drawing.Size(129, 23);
            this.btn_LMKeyLinkedin.TabIndex = 7;
            this.btn_LMKeyLinkedin.Text = "Save";
            this.btn_LMKeyLinkedin.UseVisualStyleBackColor = true;
            this.btn_LMKeyLinkedin.Click += new System.EventHandler(this.btn_LMKeyLinkedin_Click);
            // 
            // btn_CheckCredentialsLinkedin
            // 
            this.btn_CheckCredentialsLinkedin.Location = new System.Drawing.Point(36, 123);
            this.btn_CheckCredentialsLinkedin.Name = "btn_CheckCredentialsLinkedin";
            this.btn_CheckCredentialsLinkedin.Size = new System.Drawing.Size(129, 23);
            this.btn_CheckCredentialsLinkedin.TabIndex = 6;
            this.btn_CheckCredentialsLinkedin.Text = "Check Credentials";
            this.btn_CheckCredentialsLinkedin.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tb_PasswdLinkedin);
            this.groupBox4.Controls.Add(this.tb_EmailLinkedin);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(36, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(272, 111);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            // 
            // tb_PasswdLinkedin
            // 
            this.tb_PasswdLinkedin.Location = new System.Drawing.Point(65, 61);
            this.tb_PasswdLinkedin.Name = "tb_PasswdLinkedin";
            this.tb_PasswdLinkedin.Size = new System.Drawing.Size(184, 20);
            this.tb_PasswdLinkedin.TabIndex = 3;
            // 
            // tb_EmailLinkedin
            // 
            this.tb_EmailLinkedin.Location = new System.Drawing.Point(65, 23);
            this.tb_EmailLinkedin.Name = "tb_EmailLinkedin";
            this.tb_EmailLinkedin.Size = new System.Drawing.Size(184, 20);
            this.tb_EmailLinkedin.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Password";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Email";
            // 
            // tabDropbox
            // 
            this.tabDropbox.BackColor = System.Drawing.SystemColors.Control;
            this.tabDropbox.Controls.Add(this.btn_LMKeyDropbox);
            this.tabDropbox.Controls.Add(this.btn_CheckCredentialsDropbox);
            this.tabDropbox.Controls.Add(this.groupBox5);
            this.tabDropbox.ImageIndex = 4;
            this.tabDropbox.Location = new System.Drawing.Point(4, 23);
            this.tabDropbox.Name = "tabDropbox";
            this.tabDropbox.Size = new System.Drawing.Size(347, 164);
            this.tabDropbox.TabIndex = 4;
            this.tabDropbox.Text = "Dropbox";
            // 
            // btn_LMKeyDropbox
            // 
            this.btn_LMKeyDropbox.Location = new System.Drawing.Point(179, 123);
            this.btn_LMKeyDropbox.Name = "btn_LMKeyDropbox";
            this.btn_LMKeyDropbox.Size = new System.Drawing.Size(129, 23);
            this.btn_LMKeyDropbox.TabIndex = 7;
            this.btn_LMKeyDropbox.Text = "Save";
            this.btn_LMKeyDropbox.UseVisualStyleBackColor = true;
            this.btn_LMKeyDropbox.Click += new System.EventHandler(this.btn_LMKeyDropbox_Click);
            // 
            // btn_CheckCredentialsDropbox
            // 
            this.btn_CheckCredentialsDropbox.Location = new System.Drawing.Point(36, 123);
            this.btn_CheckCredentialsDropbox.Name = "btn_CheckCredentialsDropbox";
            this.btn_CheckCredentialsDropbox.Size = new System.Drawing.Size(129, 23);
            this.btn_CheckCredentialsDropbox.TabIndex = 6;
            this.btn_CheckCredentialsDropbox.Text = "Check Credentials";
            this.btn_CheckCredentialsDropbox.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tb_PasswdDropbox);
            this.groupBox5.Controls.Add(this.tb_EmailDropbox);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Location = new System.Drawing.Point(36, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(272, 111);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            // 
            // tb_PasswdDropbox
            // 
            this.tb_PasswdDropbox.Location = new System.Drawing.Point(65, 61);
            this.tb_PasswdDropbox.Name = "tb_PasswdDropbox";
            this.tb_PasswdDropbox.Size = new System.Drawing.Size(184, 20);
            this.tb_PasswdDropbox.TabIndex = 3;
            // 
            // tb_EmailDropbox
            // 
            this.tb_EmailDropbox.Location = new System.Drawing.Point(65, 23);
            this.tb_EmailDropbox.Name = "tb_EmailDropbox";
            this.tb_EmailDropbox.Size = new System.Drawing.Size(184, 20);
            this.tb_EmailDropbox.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Password";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Email";
            // 
            // ilst_Icons
            // 
            this.ilst_Icons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilst_Icons.ImageStream")));
            this.ilst_Icons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilst_Icons.Images.SetKeyName(0, "facebook-logo.png");
            this.ilst_Icons.Images.SetKeyName(1, "Gmail-Icon.png");
            this.ilst_Icons.Images.SetKeyName(2, "yahoo-mail.png");
            this.ilst_Icons.Images.SetKeyName(3, "linkedinblog.png");
            this.ilst_Icons.Images.SetKeyName(4, "dropbox-icon-256.jpg");
            this.ilst_Icons.Images.SetKeyName(5, "download (1).jpg");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(379, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.accountSettingsToolStripMenuItem,
            this.manageSitesToolStripMenuItem,
            this.toolStripSeparator1,
            this.logoutToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // accountSettingsToolStripMenuItem
            // 
            this.accountSettingsToolStripMenuItem.Name = "accountSettingsToolStripMenuItem";
            this.accountSettingsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.accountSettingsToolStripMenuItem.Text = "Account Settings";
            // 
            // manageSitesToolStripMenuItem
            // 
            this.manageSitesToolStripMenuItem.Name = "manageSitesToolStripMenuItem";
            this.manageSitesToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.manageSitesToolStripMenuItem.Text = "Manage Sites";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(161, 6);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // frmSitesManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 250);
            this.Controls.Add(this.tab_SitesManager);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmSitesManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SitesManager";
            this.Load += new System.EventHandler(this.frmSitesManager_Load);
            this.tab_SitesManager.ResumeLayout(false);
            this.tabFacebook.ResumeLayout(false);
            this.gb_FB.ResumeLayout(false);
            this.gb_FB.PerformLayout();
            this.tabGmail.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabYahooMail.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabLinkedIn.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabDropbox.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tab_SitesManager;
        private System.Windows.Forms.TabPage tabFacebook;
        private System.Windows.Forms.TabPage tabGmail;
        private System.Windows.Forms.ImageList ilst_Icons;
        private System.Windows.Forms.TabPage tabYahooMail;
        private System.Windows.Forms.TabPage tabLinkedIn;
        private System.Windows.Forms.TabPage tabDropbox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.GroupBox gb_FB;
        private System.Windows.Forms.Label lbl_PasswdFB;
        private System.Windows.Forms.Label lbl_EmailFB;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem accountSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageSitesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.Button btn_LMKeyFB;
        private System.Windows.Forms.Button btn_CheckCredentialsFB;
        private System.Windows.Forms.TextBox tb_PasswdFB;
        private System.Windows.Forms.TextBox tb_EmailFB;
        private System.Windows.Forms.Button tb_LMKeyGmail;
        private System.Windows.Forms.Button tb_CheckCredentialsGmail;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_PasswdGmail;
        private System.Windows.Forms.TextBox tb_UsernameGmail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_LMKeyY;
        private System.Windows.Forms.Button btn_CheckCredentialsY;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tb_PasswdY;
        private System.Windows.Forms.TextBox tb_UsernameY;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_LMKeyLinkedin;
        private System.Windows.Forms.Button btn_CheckCredentialsLinkedin;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tb_PasswdLinkedin;
        private System.Windows.Forms.TextBox tb_EmailLinkedin;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_LMKeyDropbox;
        private System.Windows.Forms.Button btn_CheckCredentialsDropbox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tb_PasswdDropbox;
        private System.Windows.Forms.TextBox tb_EmailDropbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}