﻿namespace YanisGUI
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.panUp = new System.Windows.Forms.Panel();
            this.panEdge = new System.Windows.Forms.Panel();
            this.panDown = new System.Windows.Forms.Panel();
            this.panRight = new System.Windows.Forms.Panel();
            this.panLeft = new System.Windows.Forms.Panel();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.lblDet = new System.Windows.Forms.Label();
            this.panEdge.SuspendLayout();
            this.SuspendLayout();
            // 
            // panUp
            // 
            this.panUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panUp.BackColor = System.Drawing.Color.Transparent;
            this.panUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panUp.Location = new System.Drawing.Point(126, 0);
            this.panUp.Name = "panUp";
            this.panUp.Size = new System.Drawing.Size(438, 115);
            this.panUp.TabIndex = 0;
            this.panUp.MouseLeave += new System.EventHandler(this.panUp_MouseLeave);
            this.panUp.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panUp_MouseOver);
            // 
            // panEdge
            // 
            this.panEdge.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panEdge.BackColor = System.Drawing.Color.Black;
            this.panEdge.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panEdge.BackgroundImage")));
            this.panEdge.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panEdge.Controls.Add(this.lblMiddle);
            this.panEdge.Controls.Add(this.lblDet);
            this.panEdge.Controls.Add(this.panDown);
            this.panEdge.Controls.Add(this.panRight);
            this.panEdge.Controls.Add(this.panUp);
            this.panEdge.Controls.Add(this.panLeft);
            this.panEdge.Location = new System.Drawing.Point(1, 0);
            this.panEdge.Name = "panEdge";
            this.panEdge.Size = new System.Drawing.Size(690, 394);
            this.panEdge.TabIndex = 0;
            // 
            // panDown
            // 
            this.panDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panDown.BackColor = System.Drawing.Color.Transparent;
            this.panDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panDown.Location = new System.Drawing.Point(135, 273);
            this.panDown.Name = "panDown";
            this.panDown.Size = new System.Drawing.Size(420, 121);
            this.panDown.TabIndex = 1;
            this.panDown.MouseLeave += new System.EventHandler(this.panDown_MouseLeave);
            this.panDown.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panDown_MouseOver);
            // 
            // panRight
            // 
            this.panRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panRight.BackColor = System.Drawing.Color.Transparent;
            this.panRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panRight.Location = new System.Drawing.Point(561, 89);
            this.panRight.Name = "panRight";
            this.panRight.Size = new System.Drawing.Size(129, 212);
            this.panRight.TabIndex = 1;
            this.panRight.MouseLeave += new System.EventHandler(this.panRight_MouseLeave);
            this.panRight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panRight_MouseOver);
            // 
            // panLeft
            // 
            this.panLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.panLeft.BackColor = System.Drawing.Color.Transparent;
            this.panLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panLeft.Location = new System.Drawing.Point(-1, 85);
            this.panLeft.Name = "panLeft";
            this.panLeft.Size = new System.Drawing.Size(130, 228);
            this.panLeft.TabIndex = 2;
            this.panLeft.MouseLeave += new System.EventHandler(this.panLeft_MouseLeave);
            this.panLeft.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panLeft_MouseOver);
            // 
            // lblMiddle
            // 
            this.lblMiddle.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMiddle.AutoSize = true;
            this.lblMiddle.BackColor = System.Drawing.Color.Transparent;
            this.lblMiddle.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMiddle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMiddle.Location = new System.Drawing.Point(163, 144);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(143, 73);
            this.lblMiddle.TabIndex = 3;
            this.lblMiddle.Text = "Pos";
            // 
            // lblDet
            // 
            this.lblDet.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDet.AutoSize = true;
            this.lblDet.BackColor = System.Drawing.Color.Transparent;
            this.lblDet.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDet.ForeColor = System.Drawing.SystemColors.Control;
            this.lblDet.Location = new System.Drawing.Point(395, 162);
            this.lblDet.Name = "lblDet";
            this.lblDet.Size = new System.Drawing.Size(91, 55);
            this.lblDet.TabIndex = 4;
            this.lblDet.Text = "det";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(124)))), ((int)(((byte)(203)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(691, 394);
            this.ControlBox = false;
            this.Controls.Add(this.panEdge);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainWindow";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(124)))), ((int)(((byte)(203)))));
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.panEdge.ResumeLayout(false);
            this.panEdge.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panUp;
        private System.Windows.Forms.Panel panDown;
        private System.Windows.Forms.Panel panLeft;
        private System.Windows.Forms.Panel panRight;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.Label lblDet;
        private System.Windows.Forms.Panel panEdge;
    }
}

