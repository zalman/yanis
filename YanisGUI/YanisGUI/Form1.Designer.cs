﻿namespace YanisGUI
{
    partial class frmHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_username = new System.Windows.Forms.Label();
            this.gb_LoginDetails = new System.Windows.Forms.GroupBox();
            this.btn_SignIn = new System.Windows.Forms.Button();
            this.tb_password_login = new System.Windows.Forms.TextBox();
            this.tb_username_login = new System.Windows.Forms.TextBox();
            this.lbl_password = new System.Windows.Forms.Label();
            this.gb_AccountCreation = new System.Windows.Forms.GroupBox();
            this.btn_CreateAccount = new System.Windows.Forms.Button();
            this.tb_pass2_acc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_pass1_acc = new System.Windows.Forms.TextBox();
            this.tb_username_acc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gb_LoginDetails.SuspendLayout();
            this.gb_AccountCreation.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_username
            // 
            this.lbl_username.AutoSize = true;
            this.lbl_username.Location = new System.Drawing.Point(6, 36);
            this.lbl_username.Name = "lbl_username";
            this.lbl_username.Size = new System.Drawing.Size(55, 13);
            this.lbl_username.TabIndex = 0;
            this.lbl_username.Text = "Username";
            // 
            // gb_LoginDetails
            // 
            this.gb_LoginDetails.Controls.Add(this.btn_SignIn);
            this.gb_LoginDetails.Controls.Add(this.tb_password_login);
            this.gb_LoginDetails.Controls.Add(this.tb_username_login);
            this.gb_LoginDetails.Controls.Add(this.lbl_password);
            this.gb_LoginDetails.Controls.Add(this.lbl_username);
            this.gb_LoginDetails.Location = new System.Drawing.Point(131, 112);
            this.gb_LoginDetails.Name = "gb_LoginDetails";
            this.gb_LoginDetails.Size = new System.Drawing.Size(272, 142);
            this.gb_LoginDetails.TabIndex = 1;
            this.gb_LoginDetails.TabStop = false;
            this.gb_LoginDetails.Visible = false;
            // 
            // btn_SignIn
            // 
            this.btn_SignIn.Location = new System.Drawing.Point(172, 108);
            this.btn_SignIn.Name = "btn_SignIn";
            this.btn_SignIn.Size = new System.Drawing.Size(75, 23);
            this.btn_SignIn.TabIndex = 4;
            this.btn_SignIn.Text = "Sign In";
            this.btn_SignIn.UseVisualStyleBackColor = true;
            this.btn_SignIn.Click += new System.EventHandler(this.btn_SignIn_Click);
            // 
            // tb_password_login
            // 
            this.tb_password_login.Location = new System.Drawing.Point(67, 72);
            this.tb_password_login.Name = "tb_password_login";
            this.tb_password_login.Size = new System.Drawing.Size(180, 20);
            this.tb_password_login.TabIndex = 3;
            // 
            // tb_username_login
            // 
            this.tb_username_login.Location = new System.Drawing.Point(67, 29);
            this.tb_username_login.Name = "tb_username_login";
            this.tb_username_login.Size = new System.Drawing.Size(180, 20);
            this.tb_username_login.TabIndex = 2;
            // 
            // lbl_password
            // 
            this.lbl_password.AutoSize = true;
            this.lbl_password.Location = new System.Drawing.Point(8, 75);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(53, 13);
            this.lbl_password.TabIndex = 1;
            this.lbl_password.Text = "Password";
            // 
            // gb_AccountCreation
            // 
            this.gb_AccountCreation.Controls.Add(this.btn_CreateAccount);
            this.gb_AccountCreation.Controls.Add(this.tb_pass2_acc);
            this.gb_AccountCreation.Controls.Add(this.label3);
            this.gb_AccountCreation.Controls.Add(this.tb_pass1_acc);
            this.gb_AccountCreation.Controls.Add(this.tb_username_acc);
            this.gb_AccountCreation.Controls.Add(this.label1);
            this.gb_AccountCreation.Controls.Add(this.label2);
            this.gb_AccountCreation.Location = new System.Drawing.Point(131, 112);
            this.gb_AccountCreation.Name = "gb_AccountCreation";
            this.gb_AccountCreation.Size = new System.Drawing.Size(274, 180);
            this.gb_AccountCreation.TabIndex = 2;
            this.gb_AccountCreation.TabStop = false;
            this.gb_AccountCreation.Visible = false;
            // 
            // btn_CreateAccount
            // 
            this.btn_CreateAccount.Location = new System.Drawing.Point(131, 141);
            this.btn_CreateAccount.Name = "btn_CreateAccount";
            this.btn_CreateAccount.Size = new System.Drawing.Size(116, 23);
            this.btn_CreateAccount.TabIndex = 5;
            this.btn_CreateAccount.Text = "Create Account";
            this.btn_CreateAccount.UseVisualStyleBackColor = true;
            this.btn_CreateAccount.Click += new System.EventHandler(this.btn_CreateAccount_Click);
            // 
            // tb_pass2_acc
            // 
            this.tb_pass2_acc.Location = new System.Drawing.Point(67, 105);
            this.tb_pass2_acc.Name = "tb_pass2_acc";
            this.tb_pass2_acc.Size = new System.Drawing.Size(180, 20);
            this.tb_pass2_acc.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password2";
            // 
            // tb_pass1_acc
            // 
            this.tb_pass1_acc.Location = new System.Drawing.Point(67, 66);
            this.tb_pass1_acc.Name = "tb_pass1_acc";
            this.tb_pass1_acc.Size = new System.Drawing.Size(180, 20);
            this.tb_pass1_acc.TabIndex = 3;
            // 
            // tb_username_acc
            // 
            this.tb_username_acc.Location = new System.Drawing.Point(67, 29);
            this.tb_username_acc.Name = "tb_username_acc";
            this.tb_username_acc.Size = new System.Drawing.Size(180, 20);
            this.tb_username_acc.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Password1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Username";
            // 
            // frmHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 400);
            this.Controls.Add(this.gb_AccountCreation);
            this.Controls.Add(this.gb_LoginDetails);
            this.Name = "frmHomePage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Welcome!";
            this.Load += new System.EventHandler(this.frm_HomePage_Load);
            this.gb_LoginDetails.ResumeLayout(false);
            this.gb_LoginDetails.PerformLayout();
            this.gb_AccountCreation.ResumeLayout(false);
            this.gb_AccountCreation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_username;
        private System.Windows.Forms.GroupBox gb_LoginDetails;
        private System.Windows.Forms.TextBox tb_username_login;
        private System.Windows.Forms.Label lbl_password;
        private System.Windows.Forms.GroupBox gb_AccountCreation;
        private System.Windows.Forms.Button btn_CreateAccount;
        private System.Windows.Forms.TextBox tb_pass2_acc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_pass1_acc;
        private System.Windows.Forms.TextBox tb_username_acc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_SignIn;
        private System.Windows.Forms.TextBox tb_password_login;
    }
}

