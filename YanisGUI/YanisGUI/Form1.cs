﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace YanisGUI
{
    public partial class frmHomePage : Form
    {
        public frmHomePage()
        {
            InitializeComponent();
        }

        

        private string getDBString(string sqlFieldName, MySqlDataReader reader)
        {
            return reader[sqlFieldName].Equals(DBNull.Value) ? String.Empty : reader.GetString(sqlFieldName);
        }

        private bool checkIfAccountExits()
        {
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using(MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"SELECT * FROM users";
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        return false;
                    }

                    reader.Close();
                    connection.Close();
                    return true;
                }
            }
        }

        private int getAuthState()
        {
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using(MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"SELECT * FROM users";
                    MySqlDataReader reader = cmd.ExecuteReader();

                    if (!reader.HasRows)
                    {
                        return -1;
                    }

                    reader.Read();
                    int state = reader["is_authenticated"].ToString() == "True" ? 1 : 0;
                    reader.Close();

                    return state;
                }
            }
        }

        private void frm_HomePage_Load(object sender, EventArgs e)
        {
            bool accExists = checkIfAccountExits();

            if (accExists)
            {
                int authState = getAuthState();
                if(authState == 1)
                {
                    frmSitesManager sitesManager = new frmSitesManager();
                    this.Visible = false;
                    sitesManager.ShowDialog();
                    this.Close();
                }
            }

            gb_LoginDetails.Visible = accExists;
            gb_AccountCreation.Visible = !accExists;

            tb_password_login.Text = tb_pass1_acc.Text = tb_pass2_acc.Text = "";
            tb_password_login.PasswordChar = tb_pass1_acc.PasswordChar = tb_pass2_acc.PasswordChar = '●';
        }

        private void addUserToDB(string username, string password)
        {
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText =
                        @"INSERT INTO users(username, password, is_authenticated) VALUES (@username, @password, @is_authenticated) ";
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", password);
                    cmd.Parameters.AddWithValue("@is_authenticated", "1");

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void btn_CreateAccount_Click(object sender, EventArgs e)
        {
            string username = tb_username_acc.Text;
            string passwd1 = PasswordManager.EncryptPassword(tb_pass1_acc.Text);
            string passwd2 = PasswordManager.EncryptPassword(tb_pass2_acc.Text);

            if (username.Equals(""))
            {
                MessageBox.Show("The username field cannot be empty!");
            }
            else
            {
                if (!passwd1.Equals(passwd2))
                {
                    MessageBox.Show("The two passwords must be the same!");
                    tb_pass1_acc.Select();
                }
                else
                {
                    addUserToDB(username, passwd1);         
                    frmSitesManager sitesManager = new frmSitesManager();
                    this.Visible = false;
                    sitesManager.ShowDialog();
                    this.Close();
                }
            }
        }

        private void updateAuthState(string state)
        {
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText =
                        @"UPDATE users SET is_authenticated=@auth_state";
                    cmd.Parameters.AddWithValue("@auth_state", state);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void btn_SignIn_Click(object sender, EventArgs e)
        {
            updateAuthState("1");
            frmSitesManager sitesManager = new frmSitesManager();
            this.Visible = false;
            sitesManager.ShowDialog();
            this.Close();
        }
    }
}
