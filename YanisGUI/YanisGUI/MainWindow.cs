﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Leap;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace YanisGUI
{
    public partial class MainWindow : Form
    {
        public delegate void PassDone();
        public event PassDone OnPassDone;

        public delegate void NewMessageDelegate(string NewMessage);
        private PipeServer _pipeServer;

        private LeapListener listener;
        private Controller controller;

        private static string root =  Application.StartupPath;
        private Boolean fromOut = true;
        private int[] pass = new int[30];
        private int length = 0;
        private bool val;
        private string passwd = string.Empty;

        public MainWindow()
        {
            InitializeComponent();
            // Create a sample listener and controller
            listener = new LeapListener();            
            controller = new Controller();

            // Have the sample listener receive events from the controller
            controller.AddListener(listener);

            Control control = new Control(controller, listener);
            control.OnLabelUpdate += new Control.UpdateLabel(updater_OnLabelUpdate);

            _pipeServer = new PipeServer();
            _pipeServer.PipeMessage += new DelegateMessage(PipesMessageHandler);
            _pipeServer.Listen("TestPipe");
            val = false;
        }

        public MainWindow(bool val)
        {
            InitializeComponent();
            // Create a sample listener and controller
            listener = new LeapListener();            
            controller = new Controller();

            // Have the sample listener receive events from the controller
            controller.AddListener(listener);

            Control control = new Control(controller, listener);
            control.OnLabelUpdate += new Control.UpdateLabel(updater_OnLabelUpdate);
            this.val = val;
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Remove the sample listener when done
            controller.RemoveListener(listener);
            controller.Dispose();

            _pipeServer.PipeMessage -= new DelegateMessage(PipesMessageHandler);
            _pipeServer = null;
        }

        private void panUp_MouseOver(object sender, MouseEventArgs e)
        {
            if (fromOut)
            {
                lblMiddle.Text = "Up";
                panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\RedOn.png");
                fromOut = false;
                pass[length] = 1;
                length++;
            }
        }

        private void panUp_MouseLeave(object sender, EventArgs e)
        {
            panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\AllOff.png");
            fromOut = true;            
        }

        private void panDown_MouseOver(object sender, MouseEventArgs e)
        {
            if (fromOut)
            {
                lblMiddle.Text = "Down";
                panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\GreenOn.png");
                fromOut = false;
                pass[length] = 3;
                length++;
            }
        }

        private void panDown_MouseLeave(object sender, EventArgs e)
        {
            panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\AllOff.png");
            fromOut = true;
        }

        private void panRight_MouseOver(object sender, MouseEventArgs e)
        {
            if (fromOut)
            {
                lblMiddle.Text = "Right";
                panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\YellowOn.png");
                fromOut = false;
                pass[length] = 2;
                length++;
            }
        }

        private void panRight_MouseLeave(object sender, EventArgs e)
        {
            panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\AllOff.png");
            fromOut = true;
        }

        private void panLeft_MouseOver(object sender, MouseEventArgs e)
        {
            if (fromOut)
            {
                lblMiddle.Text = "Left";
                panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\BlueOn.png");
                fromOut = false;
                pass[length] = 4;
                length++;
            }
        }

        private void panLeft_MouseLeave(object sender, EventArgs e)
        {
            panEdge.BackgroundImage = Image.FromFile(root + "\\assets\\AllOff.png");
            fromOut = true;
        } 

        void updater_OnLabelUpdate()
        {
            if (length != 0)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(updater_OnLabelUpdate));
                }
                else
                {
                    this.passwd = "";
                    this.lblDet.Text = "Pass: ";
                    for (int i = 0; i < length; i++)
                    {
                        this.lblDet.Text += pass[i];
                        this.passwd += pass[i];
                    }
                    length = 0;

                    if (val)
                    {
                        OnPassDone();
                        val = true;
                    }
                    else
                    {
                        login();
                    }

                }
            }
        }

        public string Password 
        {
            get { return passwd; }
        }

        private void login()
        {
            string website, usernane="", pass="";

            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"SELECT * FROM login_details where leap_key="+passwd;
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        return;
                    }
                    else
                    {
                        reader.Read();
                        website = reader["website"].ToString();
                        usernane = reader["username"].ToString();
                        pass = reader["password"].ToString();

                        LoginDriver ld = new LoginDriver();
                        ld.login(Settings.LOGIN_URLS[website], usernane, pass, Settings.XPATHS[website]);
                    }

                    reader.Close();
                    connection.Close();
                }
            }
        }

        private void PipesMessageHandler(string message)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new NewMessageDelegate(PipesMessageHandler), message);
                }
                else
                {
                    frmHomePage homePage = new frmHomePage();
                    homePage.TopMost = true;
                    homePage.ShowDialog();

                    this.Hide();

                    string text = "!!! " + message + " !!!";
                    //lblPipe.Text = text;
                    this.WindowState = FormWindowState.Minimized;
                    this.WindowState = FormWindowState.Maximized;
                }
            }
            catch (Exception ex)
            {

                Debug.WriteLine(ex.Message);
            }

        }
    }
}
