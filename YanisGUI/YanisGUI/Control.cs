﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Leap;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace YanisGUI
{
    public class Control
    {
        public delegate void UpdateLabel();
        public event UpdateLabel OnLabelUpdate;

        private static Controller _controller;
        private static LeapListener _listener;
        private bool _enableMouse;
        public double mX, mY;

        public Control(Controller controller, LeapListener listener)
        {
            _listener = listener;
            _controller = controller;
            RegisterEvents();
            _enableMouse = true;
        }

        void RegisterEvents()
        {
            _listener.OnFingersRegistered += OnFingersRegistered;
            _listener.OnGestureMade += OnGestureMade;
        }

        public void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            _controller.RemoveListener(_listener);
            _controller.Dispose();
            _listener.Dispose();
        }

        void OnGestureMade(GestureList gestures)
        {
            string temp;
            foreach (var gesture in gestures)
            {
                temp = LeapGestures.GestureTypesLookUp[gesture.Type];
                if(temp.StartsWith("Circle"))                        
                    OnLabelUpdate();
            }
        }

        void OnFingersRegistered(FingerList fingers)
        {
            var screen = _controller.LocatedScreens.ClosestScreenHit(fingers[0]);
            var coordinate = CalculationsHelper.GetNormalizedXAndY(fingers, screen);

            mX = coordinate.X;
            mY = coordinate.Y;
           
            SetLeapAsMouse(fingers, coordinate);
        }
        
        void SetLeapAsMouse(FingerList fingers, Point coordinate)
        {
            if (!_enableMouse) return;
            var screen = _controller.LocatedScreens.ClosestScreenHit(fingers[0]);
            if (screen == null || !screen.IsValid) return;
            EnableLeapAsCursor(coordinate, fingers);
            EnableClickWithLeap(coordinate, fingers);
        }

        static void EnableLeapAsCursor(Point coordinate, FingerList fingers)
        {
            if ((int)fingers[0].TipVelocity.Magnitude <= 25) return;
            LeapAsMouse.SetCursorPos((int)coordinate.X, (int)coordinate.Y);
        }

        static void EnableClickWithLeap(Point coordinate, FingerList fingers)
        {
            if (fingers.Count != 2) return;
            LeapAsMouse.mouse_event(0x0002 | 0x0004, 0, (int)coordinate.X, (int)coordinate.Y, 0);
        }

    }
}
