﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YanisGUI
{
    public static class Settings
    {
        public static Dictionary<string, string> TEST_USERS = new Dictionary<string, string>()
        {
            {"facebook", "7d8roeqp@dispostable.com"},
            {"gmail", "7d8roeqp@gmail.com"},
            {"yahoo_mail", "a7d8roeqp@yahoo.com"},
            {"linkedin", "7d8roeqp@dispostable.com"},
            {"dropbox", "7d8roeqp@dispostable.com"},
        };

        public static string TEST_PASSWORD = "asdfg123456";
        public static string YAHOO_TEST_PASSWD = "Asdfg123456";

        public static Dictionary<string, string> LOGIN_URLS = new Dictionary<string, string>()
        {
            {"facebook", "http://www.facebook.com" }, 
            {"gmail", "https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/&hl=en"}, 
            {"yahoo_mail" ,"https://login.yahoo.com/config/login_verify2?&.src=ym&.intl=us"}, 
            {"linkedin", "https://www.linkedin.com/uas/login?goback=&trk=hb_signin"},
            {"dropbox", "https://www.dropbox.com/login?lhs_type=anywhere"},
        };

        public static Dictionary<string, Dictionary<string, string>> XPATHS = new Dictionary<string, Dictionary<string, string>>()
        {
            {"facebook", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"email\"]"},
                {"password_input", "//input[@name=\"pass\"]"}, 
                {"submit_btn_input", "//input[@id=\"u_0_f\"]"},
            }},
            {"gmail", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"Email\"]"},
                {"password_input", "//input[@name=\"Passwd\"]"}, 
                {"submit_btn_input", "//input[@name=\"signIn\"]"},
            }},
            {"yahoo_mail", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"login\"]"},
                {"password_input", "//input[@name=\"passwd\"]"}, 
                {"submit_btn_input", "//button[@name=\".save\"]"},
            }},
            {"linkedin", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"session_key\"]"},
                {"password_input", "//input[@name=\"session_password\"]"}, 
                {"submit_btn_input", "//input[@name=\"signin\"]"},
            }},
            {"dropbox", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"login_email\"]"},
                {"password_input", "//input[@name=\"login_password\"]"}, 
                {"submit_btn_input", "//input[@id=\"login_submit\"]"},
            }},
        };
    }
}
