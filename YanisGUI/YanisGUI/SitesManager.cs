﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace YanisGUI
{
    public partial class frmSitesManager : Form
    {
        private MainWindow window;
        private string site;

        public frmSitesManager()
        {
            InitializeComponent();

            
        }

        private void tabYahooMail_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private string getWebsiteUsernameOrPassword(string website, string column)
        {
            string username = string.Empty;
            
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"SELECT * FROM login_details where website=@site";
                    cmd.Parameters.AddWithValue("@site", website);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        return string.Empty;
                    }
                    else
                    {
                        reader.Read();
                        username = reader[column].ToString();
                    }

                    reader.Close();
                    connection.Close();
                    return username;
                }
            }
        }

        private string getWebsiteUsername(string website)
        {
            return getWebsiteUsernameOrPassword(website, "username");
        }

        private string getWebsitePassword(string website)
        {
            return getWebsiteUsernameOrPassword(website, "password");
        }

        private void initTBs()
        {
            tb_EmailDropbox.Text = getWebsiteUsername("dropbox");
            tb_EmailFB.Text = getWebsiteUsername("facebook");
            tb_EmailLinkedin.Text = getWebsiteUsername("linkedin");
            tb_UsernameGmail.Text = getWebsiteUsername("gmail");
            tb_UsernameY.Text = getWebsiteUsername("yahoo_mail");

            tb_PasswdDropbox.Text = getWebsitePassword("dropbox");
            tb_PasswdFB.Text = getWebsitePassword("facebook");
            tb_PasswdGmail.Text = getWebsitePassword("gmail");
            tb_PasswdLinkedin.Text = getWebsitePassword("linkedin");
            tb_PasswdY.Text = getWebsitePassword("yahoo_mail");

            tb_PasswdDropbox.PasswordChar = '●';
            tb_PasswdFB.PasswordChar = '●';
            tb_PasswdGmail.PasswordChar = '●';
            tb_PasswdLinkedin.PasswordChar = '●';
            tb_PasswdY.PasswordChar = '●';

            
            window = new MainWindow(true);
            window.OnPassDone += new MainWindow.PassDone(getPass);
        }

        //private void loadSitesData()
        //{
        //    using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
        //    {
        //        using (MySqlCommand cmd = connection.CreateCommand())
        //        {
        //            connection.Open();
        //            cmd.CommandText = @"SELECT * FROM users";
        //            MySqlDataReader reader = cmd.ExecuteReader();
        //            if (!reader.HasRows)
        //            {
        //                return false;
        //            }

        //            reader.Close();
        //            connection.Close();
        //            return true;
        //        }
        //    }
 
        //}

        private void frmSitesManager_Load(object sender, EventArgs e)
        {
            initTBs();
        }

        private bool checkIfWebsiteExists(string website)
        {
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"SELECT * FROM login_details WHERE website=@site;";
                    cmd.Parameters.AddWithValue("@site", website);
                    MySqlDataReader reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                    {
                        return false;
                    }

                    reader.Close();
                    connection.Close();
                    return true;
                }
            }
        }

        private void addWebsiteInfoToDB(string website, string username, string password, string LeapKey)
        {
            
            using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
            {
                using (MySqlCommand cmd = connection.CreateCommand())
                {
                    connection.Open();
                    cmd.CommandText = @"INSERT INTO login_details(website, username, password, leap_key) VALUES (@site, @user, @passwd, @key)";
                    cmd.Parameters.AddWithValue("@site", website);
                    cmd.Parameters.AddWithValue("@user", username);
                    cmd.Parameters.AddWithValue("@passwd", password);
                    cmd.Parameters.AddWithValue("@key", LeapKey);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        //private void updateWebsiteInfo(string website, string username, string password)
        //{
        //    using (MySqlConnection connection = new MySqlConnection(DBManager.conString))
        //    {
        //        using (MySqlCommand cmd = connection.CreateCommand())
        //        {
        //            connection.Open();
        //            cmd.CommandText = @"";
        //            cmd.Parameters.AddWithValue("@site", website);
        //            cmd.Parameters.AddWithValue("@user", username);
        //            cmd.Parameters.AddWithValue("@passwd", password);
        //            cmd.Parameters.AddWithValue("@key", LeapKey);
        //            cmd.ExecuteNonQuery();
        //        }
        //    }
        //}

        private void btn_LMKeyFB_Click(object sender, EventArgs e)
        {
            site = "facebook";
            window.ShowDialog();
        }
        
        private void tb_LMKeyGmail_Click(object sender, EventArgs e)
        {
            site = "gmail";
            window.ShowDialog();
        }

        private void btn_LMKeyY_Click(object sender, EventArgs e)
        {
            site = "yahoo_mail";
            window.ShowDialog();
        }

        private void btn_LMKeyLinkedin_Click(object sender, EventArgs e)
        {
            site = "linkedin";
            window.ShowDialog();
        }

        private void btn_LMKeyDropbox_Click(object sender, EventArgs e)
        {
            site = "dropbox";
            window.ShowDialog();
        }

        private void getPass()
        {
            string LeapMotionKey = window.Password;
            MessageBox.Show(LeapMotionKey);

            switch (site)
            {
                case "facebook":
                    addWebsiteInfoToDB("facebook", tb_EmailFB.Text, tb_PasswdFB.Text, LeapMotionKey);
                    break;
                case "gmail":
                    addWebsiteInfoToDB("gmail", tb_UsernameGmail.Text, tb_PasswdGmail.Text, LeapMotionKey);
                    break;
                case "yahoo_mail":
                    addWebsiteInfoToDB("yahoo_mail", tb_UsernameY.Text, tb_PasswdY.Text, LeapMotionKey);
                    break;
                case "linkedin":
                    addWebsiteInfoToDB("linkedin", tb_EmailLinkedin.Text, tb_PasswdLinkedin.Text, LeapMotionKey);
                    break;
                case "dropbox":
                    addWebsiteInfoToDB("dropbox", tb_EmailDropbox.Text, tb_PasswdDropbox.Text, LeapMotionKey);
                    break;
            }

            MessageBox.Show("The login details have been successfully saved!");
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }
    }
}
