﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using IronPython.Modules;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Microsoft.Win32;

namespace LogInTool
{
    public class DATA
    {
        public static Dictionary<string, string> TEST_USERS = new Dictionary<string, string>()
        {
            {"facebook", "7d8roeqp@dispostable.com"},
            {"gmail", "7d8roeqp@gmail.com"},
            {"yahoo_mail", "a7d8roeqp@yahoo.com"},
            {"linkedin", "7d8roeqp@dispostable.com"},
            {"dropbox", "7d8roeqp@dispostable.com"},
        };

        public static string TEST_PASSWORD = "asdfg123456";
        public static string YAHOO_TEST_PASSWD = "Asdfg123456";

        public static Dictionary<string, string> ACCEPTED_URLS = new Dictionary<string, string>()
        {
            {"facebook", "http://www.facebook.com" }, 
            {"gmail", "https://accounts.google.com/ServiceLogin?service=mail&continue=https://mail.google.com/mail/&hl=en"}, 
            {"yahoo_mail" ,"https://login.yahoo.com/config/login_verify2?&.src=ym&.intl=us"}, 
            {"linkedin", "https://www.linkedin.com/uas/login?goback=&trk=hb_signin"},
            {"dropbox", "https://www.dropbox.com/login?lhs_type=anywhere"},
        }; 

        public static Dictionary<string, Dictionary<string, string>> XPATHS = new Dictionary<string, Dictionary<string, string>>()
        {
            {"facebook", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"email\"]"},
                {"password_input", "//input[@name=\"pass\"]"}, 
                {"submit_btn_input", "//input[@id=\"u_0_f\"]"},
            }},
            {"gmail", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"Email\"]"},
                {"password_input", "//input[@name=\"Passwd\"]"}, 
                {"submit_btn_input", "//input[@name=\"signIn\"]"},
            }},
            {"yahoo_mail", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"login\"]"},
                {"password_input", "//input[@name=\"passwd\"]"}, 
                {"submit_btn_input", "//button[@name=\".save\"]"},
            }},
            {"linkedin", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"session_key\"]"},
                {"password_input", "//input[@name=\"session_password\"]"}, 
                {"submit_btn_input", "//input[@name=\"signin\"]"},
            }},
            {"dropbox", new Dictionary<string, string>()
            {
                {"username_input", "//input[@name=\"login_email\"]"},
                {"password_input", "//input[@name=\"login_password\"]"}, 
                {"submit_btn_input", "//input[@id=\"login_submit\"]"},
            }},
        };

    }
    class LoginDriver
    {
        private string chromeDriverLocation = @"C:\Users\Manu\Downloads\chromedriver_win32\";
        private string firefoxPath = (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\firefox.exe", "", null)).ToString();

        public LoginDriver()
        {
        }

        public void login(string baseUrl, string username, string password,
            Dictionary<string, string> xpaths)
        {

            FirefoxProfile profile = new FirefoxProfile();
            profile.SetPreference("webdriver.firefox.bin", this.firefoxPath);
            IWebDriver driver = new FirefoxDriver(profile);
            driver.Manage().Window.Maximize();
            
            driver.Navigate().GoToUrl(baseUrl);

            driver.FindElement(By.XPath(xpaths["username_input"])).Clear();
            driver.FindElement(By.XPath(xpaths["username_input"])).SendKeys(username);

            driver.FindElement(By.XPath(xpaths["password_input"])).Clear();
            driver.FindElement(By.XPath(xpaths["password_input"])).SendKeys(password);

            driver.FindElement(By.XPath(xpaths["submit_btn_input"])).Click();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            LoginDriver ld = new LoginDriver();
            //ld.login(DATA.ACCEPTED_URLS["facebook"], DATA.TEST_USERS["facebook"], DATA.TEST_PASSWORD, DATA.XPATHS["facebook"]);
            //ld.login(DATA.ACCEPTED_URLS["gmail"], DATA.TEST_USERS["gmail"], DATA.TEST_PASSWORD, DATA.XPATHS["gmail"]);
            //ld.login(DATA.ACCEPTED_URLS["yahoo_mail"], DATA.TEST_USERS["yahoo_mail"], DATA.YAHOO_TEST_PASSWD, DATA.XPATHS["yahoo_mail"]);
            //ld.login(DATA.ACCEPTED_URLS["linkedin"], DATA.TEST_USERS["linkedin"], DATA.TEST_PASSWORD, DATA.XPATHS["linkedin"]);
            ld.login(DATA.ACCEPTED_URLS["dropbox"], DATA.TEST_USERS["dropbox"], DATA.TEST_PASSWORD, DATA.XPATHS["dropbox"]);
        }
    }
}
