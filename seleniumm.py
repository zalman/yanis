from selenium import webdriver

#Following are optional required
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

TEST_USERS = {
    'facebook': '7d8roeqp@dispostable.com',
    'google': '7d8roeqp@gmail.com',
}
TEST_PASSWORD = 'asdfg123456'

ACCEPTED_URLS = {
    'facebook': "http://www.facebook.com",
    'google':  'https://accounts.google.com/',
}

XPATHS = {
    'facebook': { 
        'username_input' : "//input[@name='email']",
        'password_input' : "//input[@name='pass']",
        'submit_btn_input' : "//input[@id='u_0_f']",
    },
    'google': {
        'username_input' : "//input[@name='Email']",
        'password_input' : "//input[@name='Passwd']",
        'submit_btn_input' : "//input[@name='signIn']",
    }
}


def login(base_url, username, password, xpaths):
    
    mydriver = webdriver.Firefox()
    mydriver.get(base_url)
    mydriver.maximize_window()
    
    #Clear Username text_box if already allowed "Remember Me" 
    mydriver.find_element_by_xpath(xpaths['username_input']).clear()
    
    #Write Username in Username TextBox
    mydriver.find_element_by_xpath(xpaths['username_input']).send_keys(username)
    
    #Clear Password TextBox if already allowed "Remember Me" 
    mydriver.find_element_by_xpath(xpaths['password_input']).clear()
    
    #Write Password in password TextBox
    mydriver.find_element_by_xpath(xpaths['password_input']).send_keys(password)
    
    #Click Login button
    mydriver.find_element_by_xpath(xpaths['submit_btn_input']).click()
    
if __name__ == "__main__":
    login(ACCEPTED_URLS['facebook'], TEST_USERS['facebook'], TEST_PASSWORD, XPATHS['facebook'])
    
    
    